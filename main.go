package main

import (
	"context"
	"fmt"
	consul "github.com/hashicorp/consul/api"
	"github.com/phayes/freeport"
	"gitlab.com/vakoto/museums-tour-tools/health_check"
	"gitlab.com/vakoto/museums-tour-tools/model"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
)

type server struct {
	mu        sync.Mutex
	statusMap map[string]grpc_health_v1.HealthCheckResponse_ServingStatus
}

func NewServer() *server {
	statusMap := map[string]grpc_health_v1.HealthCheckResponse_ServingStatus{
		"grpc.health.v1.Health": grpc_health_v1.HealthCheckResponse_SERVING,
	}
	return &server{
		statusMap: statusMap,
	}
}

func (s *server) Check(ctx context.Context, in *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if in.Service == "" {
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_SERVING,
		}, nil
	}
	if status, ok := s.statusMap[in.Service]; ok {
		return &grpc_health_v1.HealthCheckResponse{
			Status: status,
		}, nil
	}
	return nil, status.Error(codes.NotFound, "unknown service")
}

func (s *server) GetPath(ctx context.Context, in *model.GetPathRequest) (*model.GetPathReply, error) {
	if peer, ok := peer.FromContext(ctx); ok {
		log.Printf("Path find request from %s", peer.Addr.String())
	}
	path, dist, err := getPath(in.MuseumIds)

	if err != nil {
		return nil, err
	}

	return &model.GetPathReply{
		Path:     path,
		Distance: float64(dist),
	}, err
}

func main() {
	args := os.Args
	if (len(args) < 2) {
		log.Fatalf("First argument should be the server address")
	}
	address := args[1]

	port, err := freeport.GetFreePort()

	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("Failed to listen: %s", err)
	}
	defer listener.Close()
	log.Printf("Listening on :%d", port)

	server := NewServer()
	s := grpc.NewServer()
	model.RegisterPathFinderServer(s, server)
	grpc_health_v1.RegisterHealthServer(s, server)

	config := consul.DefaultConfig()
	config.Address = consulAddress
	consulClient, err := consul.NewClient(config)
	if err != nil {
		log.Fatalf("Failed to connect to the consul server: %s", err)
	}

	serviceId := "solver:" + address + ":" + strconv.Itoa(port)
	serviceName := "solver"

	reg := &consul.AgentServiceRegistration{
		ID:      serviceId,
		Name:    serviceName,
		Port:    port,
		Address: address,
		Checks: consul.AgentServiceChecks{
			&consul.AgentServiceCheck{
				CheckID:    "solverHealthCheck:" + serviceId,
				Name:       "Solver service health status",
				GRPC:       fmt.Sprintf("%s:%d", address, port),
				GRPCUseTLS: false,
				Interval:   "10s",
			},
		},
	}
	err = consulClient.Agent().ServiceRegister(reg)
	if err != nil {
		log.Fatalf("Failed to register service: %s", err)
	}
	log.Printf("Service %s successfully registered", serviceId)

	defer func() {
		err := consulClient.Agent().ServiceDeregister(serviceId)
		if err != nil {
			log.Printf("Failed to deregister the solver %s: %s", serviceId, err)
			return
		}
		log.Printf("Deregistered %s in consul", serviceId)
	}()

	reflection.Register(s)
	go func() {
		s.Serve(listener)
	}()

	signalChannel := make(chan os.Signal, 2)
	done := make(chan bool, 1)
	signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)
	go func() {
		sig := <-signalChannel
		switch sig {
		case os.Interrupt, syscall.SIGTERM:
			done <- true
		}
	}()

	<-done
}
