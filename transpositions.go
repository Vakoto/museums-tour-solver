package main

import (
	"math"
)

type element struct {
	value int
	dir   int
}

func getTranspositionWithMinPrice(n int, getPrice func([]element) (int)) ([]int, int, error) {
	transposition := make([]element, 0)
	for i := 0; i < n; i++ {
		transposition = append(transposition, element{i, -1})
	}
	bestTransposition := make([]element, len(transposition))
	bestPrice := math.MaxInt32
	for {
		currentPrice := getPrice(transposition)

		if bestPrice > currentPrice {
			bestPrice = currentPrice
			copy(bestTransposition, transposition)
		}

		largestIndex := getLargestMobileIndex(transposition)
		if largestIndex == -1 {
			break
		}
		neighborIndex := largestIndex + transposition[largestIndex].dir

		transposition[largestIndex], transposition[neighborIndex] = transposition[neighborIndex], transposition[largestIndex]

		changeLargerElementsDir(transposition, transposition[neighborIndex].value)
	}

	bestPath := make([]int, 0)
	for _, element := range bestTransposition {
		bestPath = append(bestPath, element.value)
	}

	return bestPath, bestPrice, nil
}

func isMobile(transposition []element, i int) bool {
	if i == 0 && transposition[i].dir == -1 {
		return false
	}

	if i == len(transposition)-1 && transposition[i].dir == 1 {
		return false
	}

	neighborIndex := i + transposition[i].dir

	return transposition[i].value > transposition[neighborIndex].value
}

func getLargestMobileIndex(transposition []element) int {
	largestIndex := -1
	largestValue := 0

	for i, element := range transposition {
		if isMobile(transposition, i) && element.value > largestValue {
			largestIndex = i
			largestValue = element.value
		}
	}

	return largestIndex
}

func changeLargerElementsDir(transposition []element, value int) {
	for i, element := range transposition {
		if element.value > value {
			transposition[i].dir = -transposition[i].dir
		}
	}
}
