package main

import (
	"context"
	"fmt"
	"github.com/kr/pretty"
	"gitlab.com/vakoto/museums-tour-tools/model"
	"googlemaps.github.io/maps"
	"log"
	"sync"
)

var lock sync.Mutex

func removeDuplicates(elements []int32) []int32 {
	encountered := map[int32]bool{}
	result := []int32{}

	for v := range elements {
		if encountered[elements[v]] == true {
		} else {
			encountered[elements[v]] = true
			result = append(result, elements[v])
		}
	}

	return result
}

func getPath(museumIds []int32) ([]int32, int, error) {
	// Если надо обновить матрицу расстояний, то следует раскомментить
	//if err := saveDistances("museums.csv"); err != nil {
	//	log.Fatalf("Failed to save distances to file: %s", err)
	//}

	museumIds = removeDuplicates(museumIds)

	var distances [][]int
	if err := loadDistances(&distances); err != nil {
		log.Printf("Failed to load distances from %s: %s", distancesFileName, err)
		return []int32{}, 0, err
	}

	getTotalDistance := func(transposition []element)(int) {
		distance := 0
		for i := 0; i < len(transposition) - 1; i++ {
			origin := transposition[i].value
			destination := transposition[i + 1].value
			distance += distances[origin][destination]
		}
		return distance
	}

	bestTransposition, bestDistance, err := getTranspositionWithMinPrice(len(museumIds), getTotalDistance)
	if err != nil {
		return []int32{}, 0, err
	}

	bestPath := make([]int32, 0)

	for _, i := range bestTransposition {
		bestPath = append(bestPath, museumIds[i])
	}

	return bestPath, bestDistance, nil
}

func getDistances(museums []model.Museum) ([][]int, error) {
	c, err := maps.NewClient(maps.WithAPIKey(apiKey))
	if err != nil {
		log.Printf("Failed to connect to Google Maps API: %s", err)
		return nil, err
	}

	var coordinates []string
	for _, museum := range museums {
		coordinates = append(coordinates, fmt.Sprintf("%f,%f", museum.Latitude, museum.Longitude))
	}

	distances := [][]int{}

	for _, origin := range coordinates {
		req := &maps.DistanceMatrixRequest{
			Origins:      [] string{origin},
			Destinations: coordinates,
			Mode:         "ModeDriving",
		}
		resp, err := c.DistanceMatrix(context.Background(), req)
		if err != nil {
			log.Printf("Failed to get distance matrix: %s", err)
			return nil, err
		}
		row := make([]int, 0)

		for _, element := range resp.Rows[0].Elements {
			row = append(row, element.Distance.Meters)
		}

		distances = append(distances, row)
	}

	pretty.Println(distances)

	return distances, nil
}
