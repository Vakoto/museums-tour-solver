package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"gitlab.com/vakoto/museums-tour-tools/model"
	"io"
	"os"
	"strconv"
)

var Marshal = func(v interface{}) (io.Reader, error) {
	b, err := json.MarshalIndent(v, "", "\t")
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(b), nil
}

var Unmarshal = func(r io.Reader, v interface{}) error {
	return json.NewDecoder(r).Decode(v)
}

func Save(path string, v interface{}) error {
	lock.Lock()
	defer lock.Unlock()
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()
	r, err := Marshal(v)
	if err != nil {
		return err
	}
	_, err = io.Copy(f, r)
	return err
}

func Load(path string, v interface{}) error {
	lock.Lock()
	defer lock.Unlock()
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	return Unmarshal(f, v)
}

func loadDistances(distances *[][]int) error {
	return Load(distancesFileName, distances)
}

func saveDistances(sourceCsvFileName string) error {
	museums, err := parse(sourceCsvFileName)
	if err != nil {
		return err
	}
	distances, err := getDistances(museums);
	if err != nil {
		return err
	}
	err = Save(distancesFileName, distances);
	if err != nil {
		return err
	}
	return nil
}

func parse(fileName string) ([]model.Museum, error) {
	var empty []model.Museum

	file, err := os.Open(fileName)
	if err != nil {
		return empty, err
	}

	reader := csv.NewReader(bufio.NewReader(file))
	reader.Comma = ';'

	_, err = reader.Read()
	if err != nil {
		return empty, err
	}

	var museums []model.Museum
	for {

		line, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				return empty, err
			}
		}

		id, err := strconv.Atoi(line[0])
		if err != nil {
			return empty, err
		}

		name := line[1]

		latitude, err := strconv.ParseFloat(line[2], 64)
		if err != nil {
			return empty, err
		}

		longitude, err := strconv.ParseFloat(line[3], 64)
		if err != nil {
			return empty, err
		}

		address := line[4]
		phone := line[5]
		contacts := line[6]

		museums = append(museums, model.Museum{
			Id:        int32(id),
			Name:      name,
			Latitude:  latitude,
			Longitude: longitude,
			Address:   address,
			Phone:     phone,
			Contacts:  contacts,
		})
	}

	return museums, nil
}

